from skimage import io, transform
import numpy as np
import cv2
import scipy.io

'''img = type uint8
rg = [r,g]'''


def image_correction(image, rg):
    image_corrected = np.copy(image)
    image_corrected = image_corrected.astype('float32')

    e = [rg[0], rg[1], 1 - (rg[0] + rg[1])]
    for i in range(image.shape[0]):
        for j in range(image.shape[1]):
            image_corrected[i, j] = image[i, j] / e
            if (image_corrected[i, j][0] > 255):
                image_corrected[i, j][0] = 255
            if (image_corrected[i, j][1] > 255):
                image_corrected[i, j][1] = 255
            if (image_corrected[i, j][2] > 255):
                image_corrected[i, j][2] = 255

    return image_corrected.astype('uint8')


#########################################

# img = io.imread('/content/gdrive/My Drive/VISAO/illumination/IMG_0847.tif')
# r = 0.683172
# g = 0.265516

# img = io.imread('/content/gdrive/My Drive/VISAO/illumination/IMG_0811.tif')
# r = 0.336034
# g = 0.333463

img = io.imread('/content/gdrive/My Drive/VISAO/illumination/8D5U5524.tif')
# Gerado pelo programa
# r = 0.28741147354917373
# g = 0.44720749766087686

img = io.imread('/content/gdrive/My Drive/VISAO/illumination/IMG_0686.tif')
# Gerado pelo programa
# r = 0.33278993203883495
# g = 0.330481786407767

# GT
r = 0.631052
g = 0.302334

image = image_correction(img, [r, g])
io.imshow(image)

io.imsave('/content/gdrive/My Drive/VISAO/illumination/IMG_0686.bmp', img)
io.imsave('/content/gdrive/My Drive/VISAO/illumination/IMG_0686_gd.bmp', image)